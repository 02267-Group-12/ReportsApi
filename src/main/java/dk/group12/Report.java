package dk.group12;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@Data
public class Report implements Serializable {
    public Report() {
    }

    public Report(UUID uuid, String accountId, String bankAccountId, String accountType) {
        this.accountUUID = uuid;
        this.accountId = accountId;
        this.bankAccountId = bankAccountId;
        this.accountType = accountType;
    }

    private static final long serialVersionUID = -8207466714979398085L;
    public String accountId = null;
    public String bankAccountId = null;

    @JsonIgnore
    public String accountType = "customer";
    @JsonIgnore
    public UUID accountUUID = null;
}