package dk.group12;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;
import org.jboss.logging.Logger;

import com.google.gson.Gson;

import dk.group12.events.CorrelationId;
import dk.group12.events.Event;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata;
import io.vertx.core.json.JsonObject;
import java.util.Arrays;

@ApplicationScoped
public class ReportService {

    private static final Logger LOG = Logger.getLogger(ReportService.class);

    Map<UUID, Uni<String[][]>> reportMap = new ConcurrentHashMap<>();

    @Inject
    @Channel("request-report")
    Emitter<JsonObject> requestReportQueue;

    /**
     * @author Jonas
     * @param jObj
     */
    @Incoming("report-completed")
    public void reportRequestCompleted(JsonObject jObj){
        Event ev = convertJsonToObject(jObj, Event.class);

        String[][] report = ev.getArgument(0, String[][].class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);

        reportMap.computeIfPresent(corrId.getId(), (key, val) -> {
            return val.onItem().transformToUni(a -> Uni.createFrom().item(report));
        });
    }

    /**
     * @author Jonas
     * @return
     */
    public Uni<String[][]> getReports(){
        CorrelationId correlationId = CorrelationId.randomId();
        reportMap.put(correlationId.getId(), Uni.createFrom().nullItem());
        Event ev = new Event("request-report", new Object[] { correlationId });
        JsonObject asJsonObj = convertObjectToJson(ev);
        
        OutgoingRabbitMQMetadata meta = OutgoingRabbitMQMetadata.builder()
                .withReplyTo("merchant")
                .build();

        requestReportQueue.send(Message.of(asJsonObj, Metadata.of(meta)));

        Uni<String[][]> reportUni = reportMap.get(correlationId.getId())
                .onItem().transformToUni(customer -> reportMap.get(correlationId.getId()))
                .onItem().invoke(reports -> LOG.infof("reports gotten:%s", Arrays.toString(reports)))
                // Check for failure
                .onItem().ifNull().fail()
                // Deal with Failure
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                .onItem().call(() -> reportMap.remove(correlationId.getId()));
        
        return reportUni;
    }

    private <T> T convertJsonToObject(JsonObject jObj, Class<T> classType) {
        return new Gson().fromJson(jObj.toString(), classType);
    }

    private JsonObject convertObjectToJson(Object obj) {
        return new JsonObject(new Gson().toJson(obj, obj.getClass()));
    }
}