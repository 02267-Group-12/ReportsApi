FROM maven:3.5-jdk-11 AS build  
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app  
RUN mvn -f /usr/src/app/pom.xml clean package


FROM adoptopenjdk:11-jre-hotspot
WORKDIR /usr/src
COPY --from=build /usr/src/app/target/quarkus-app /usr/src/quarkus-app
CMD java -Xmx64m -jar quarkus-app/quarkus-run.jar