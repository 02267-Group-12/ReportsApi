package dk.group12;

import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.jboss.logging.Logger;

import io.quarkus.runtime.Startup;
import io.smallrye.common.annotation.NonBlocking;
import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.core.MediaType;

@Startup
@ApplicationScoped
@Path("/report")
public class ReportResource{

    private static final Logger LOG = Logger.getLogger(ReportResource.class);


    @Inject
    ReportService rService;

    /**
     * @author Jonas
     * @return
     */
    @GET
    @NonBlocking
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<String[][]> getReports() {
        return rService.getReports();
    }

}